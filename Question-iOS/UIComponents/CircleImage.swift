//
//  CircleImage.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import UIKit

class CircleImage: UIImageView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    func setup() {
        self.clipsToBounds = true
        self.layer.cornerRadius = self.frame.height / 2
    }

}
