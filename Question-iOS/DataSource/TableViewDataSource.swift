//
//  TableViewDataSource.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation
import UIKit



public protocol CellBehavior {
    func getReuseId() -> String
}

public protocol SectionBehavior  {
    var cells : [CellBehavior] {get set}
}

public class DefaultSectionBehavior : SectionBehavior {
    public var cells: [CellBehavior]
    
    init(cells : [CellBehavior]) {
        self.cells = cells
    }
}


public class TableViewDataSource : NSObject, UITableViewDataSource , UITableViewDelegate {
    
    var sections : [SectionBehavior]
    var selectCompletion : ((IndexPath,CellBehavior) -> ())?
    var deSelectCompletion : ((IndexPath,CellBehavior) -> ())?
    
    init(sections : [SectionBehavior], cellSelected : ((IndexPath,CellBehavior) -> ())?, cellDeselected : ((IndexPath,CellBehavior) -> ())?) {
        self.sections = sections
        self.selectCompletion = cellSelected
        self.deSelectCompletion = cellDeselected
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.sections[indexPath.section].cells[indexPath.row].getReuseId()) as! BaseTableViewCell
        cell.updateCell(cellModel: self.sections[indexPath.section].cells[indexPath.row])
        return cell
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].cells.count
    }
    
    
}

