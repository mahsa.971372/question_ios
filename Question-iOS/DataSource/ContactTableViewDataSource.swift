//
//  ContactTableViewDataSource.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import UIKit

class ContactTableViewDataSource: TableViewDataSource {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selectCompletion != nil {
            self.selectCompletion!(indexPath, self.sections[indexPath.section].cells[indexPath.row])
        }
    }
    
  
}
