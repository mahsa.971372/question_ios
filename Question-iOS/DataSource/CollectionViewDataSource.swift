//
//  CollectionViewDataSource.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation
import UIKit

public class CollectionViewDataSource : NSObject , UICollectionViewDelegate , UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    
    
    var cells : [CellBehavior]

    
    init(cells : [CellBehavior]) {
        self.cells = cells
    }

    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cells[indexPath.row].getReuseId(), for: indexPath) as! BaseCollectionViewCell
        cell.updateCell(cellModel: cells[indexPath.row])
        return cell
    }
    
    public func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cells.count
    }
    
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 100, height: 100)
    }
    
}

