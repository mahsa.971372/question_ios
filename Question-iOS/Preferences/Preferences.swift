//
//  Preferences.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation


public enum PreferenceKeys : String {
    case CONTACT_ACCESS_PERMISSION = "CONTACT_ACCESS_PERMISSION"
}


public class Preferences {
    
    public static func setContactAccessPermisionHasBeenGranted() {
        UserDefaults.standard.setValue(true, forKey: PreferenceKeys.CONTACT_ACCESS_PERMISSION.rawValue)
    }
    public static func isAccessToContactAllowed() -> Bool {
        return (UserDefaults.standard.value(forKey: PreferenceKeys.CONTACT_ACCESS_PERMISSION.rawValue) as? Bool) ?? false
    }
}
