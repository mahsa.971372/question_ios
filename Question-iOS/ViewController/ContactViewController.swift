//
//  ContactViewController.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import UIKit
import IQKeyboardManager

class ContactViewController: UIViewController {
    

    @IBOutlet weak var collectionView : UICollectionView! {
        didSet{
            let label = UILabel()
            label.text = "Select contact".localized
            label.textColor = UIColor.init(named: "SubtitleColor")
            label.textAlignment = .center
            self.collectionView.backgroundView = label
            self.collectionView.register(UINib(nibName: "ContactCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CTC")
            
        }
    }
    @IBOutlet weak var tableView : UITableView! {
        didSet{
            self.tableView.allowsMultipleSelection = true
            self.tableView.rowHeight = UITableViewAutomaticDimension
            self.tableView.register(UINib.init(nibName: "ContactTableViewCell", bundle: nil), forCellReuseIdentifier: "CTC")
    
        }
    }
    @IBOutlet weak var searchBar : UISearchBar! {
        didSet{
            self.searchBar.delegate = self
            
        }
    }
    var tableViewDataSource : ContactTableViewDataSource?
    var collectionViewDataSource : CollectionViewDataSource?
    
    var selectedCells : [Int : ContactCellFactory] = [Int : ContactCellFactory]() {
        didSet {
            if selectedCells.count != 0 {
                self.collectionView.backgroundView?.isHidden = true
                
            }
            else {
                self.collectionView.backgroundView?.isHidden = false
            }
        }
    }
    var createdCells = [ContactCellFactory]()
    var searched = [ContactCellFactory]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.title = "Add Member".localized
        
        self.collectionViewDataSource = CollectionViewDataSource.init(cells: [CellBehavior]())
        setupTableView()
    }
    
    func setupTableView() {
        
        if let contacts = Contacts.loadContacts() {
            contacts.forEach { (item) in
                createdCells.append(ContactCellFactory.init(item: item, delegate : self))
            }
        }
        tableViewDataSource = ContactTableViewDataSource(sections: [DefaultSectionBehavior.init(cells: createdCells)], cellSelected: { (indexPath : IndexPath, selectedCell : CellBehavior) in
            self.createdCells[indexPath.row].selected = !self.createdCells[indexPath.row].selected
            self.tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.fade)
            if !self.createdCells[indexPath.row].selected {
                self.selectedCells.removeValue(forKey: indexPath.row)
            }
            else {
                self.selectedCells[indexPath.row] = (selectedCell as! ContactCellFactory)
            }
            self.setupCollectionView()
        }, cellDeselected: nil)
        self.tableView.delegate = tableViewDataSource
        self.tableView.dataSource = tableViewDataSource
        self.tableView.reloadData()
    }
    
    func setupCollectionView() {
        let values = self.selectedCells.values
        var cells = [ContactCellFactory]()
        values.forEach { (item) in
            cells.append(item)
        }
        self.collectionViewDataSource?.cells = cells
        self.collectionView.dataSource = self.collectionViewDataSource
        self.collectionView.delegate = self.collectionViewDataSource
        self.collectionView.reloadData()

    }
}


extension ContactViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            self.resetData()

            return
        }

        self.searched = self.createdCells.filter({ (item) -> Bool in
                item.phoneNumber.contains(searchText) || item.name.contains(searchText)
            })
        self.tableViewDataSource?.sections[0].cells = self.searched
        self.tableView.reloadData()
    }
    
    func resetData() {
        IQKeyboardManager.shared().resignFirstResponder()
        self.tableViewDataSource?.sections[0].cells = self.createdCells
        self.tableView.reloadData()
    }
    
    
    
}

extension ContactViewController : deletButtonResponder {
    func deleteButonTapped(item: ContactCellFactory) {
        if let deselectedCell = self.selectedCells.filter({ (key, value) -> Bool in
            return value.id == item.id
        }).first {
            self.selectedCells.removeValue(forKey: deselectedCell.key)
        }
        
        if let deselectedCellInTableView = self.createdCells.filter({ (x) -> Bool in
            return x.id == item.id
        }).first {
            deselectedCellInTableView.selected = false
        }
        
        self.tableView.reloadData()
        setupCollectionView()

        

    }




}


