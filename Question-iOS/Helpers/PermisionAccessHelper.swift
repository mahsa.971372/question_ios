//
//  PermisionAccessHelper.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation
import Contacts


public class PermissionAccessHelper {
    public static func accessToContact(completionHandler : @escaping (Bool) -> ()) {
        CNContactStore().requestAccess(for: CNEntityType.contacts) { (granted, error) in
            if granted {
                completionHandler(true)
            }
            else {
                completionHandler(false)
            }
        }
    }
}
