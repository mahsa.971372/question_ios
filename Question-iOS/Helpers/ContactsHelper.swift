//
//  ContactsHelper.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation
import Contacts


public class ContactHelper {
    
    static let shared = ContactHelper()
    lazy var contactStore = {
        return CNContactStore()
    }()
    public func loadContactToLocalStorage() {
       
            let keysToFetch = [CNContactImageDataKey, CNContactGivenNameKey,CNContactFamilyNameKey , CNContactPhoneNumbersKey]
            var allContainers : [CNContainer] = []
            do
            {
                allContainers = try self.contactStore.containers(matching: nil)
                var result = [CNContact]()
                for container in allContainers
                {
                    let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)
                    do
                    {
                        let containerResults = try self.contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as [CNKeyDescriptor])
                        result.append(contentsOf: containerResults)
                        self.storeFetchContactToCoreData(result: result)
                    }
                    catch
                    {
                        debugPrint("Can't fetch contacts")
                    }
                }
            }
            catch
            {
                debugPrint("Can't fetch contacts")
            }
        
    }
    
    private func storeFetchContactToCoreData(result : [CNContact]) {
        Contacts.insertContacts(array: result)
    }
    
}
