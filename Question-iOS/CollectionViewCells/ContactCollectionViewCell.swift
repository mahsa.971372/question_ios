//
//  ContactCollectionViewCell.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import UIKit

public protocol deletButtonResponder {
    func deleteButonTapped( item : ContactCellFactory)
}

class ContactCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var contactImage : UIImageView!
    @IBOutlet weak var contactName : UILabel!
    @IBOutlet weak var deleteBtn : UIButton! {
        didSet{
            self.deleteBtn.clipsToBounds = true
            self.deleteBtn.layer.cornerRadius = self.deleteBtn.frame.height/2
        }
    }
    
     var contact : ContactCellFactory?
     var delegate : deletButtonResponder?

   override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func updateCell(cellModel: CellBehavior) {
        if let model = cellModel as? ContactCellFactory {
            if let data = model.image {
                self.contactImage.image = UIImage.init(data:data)
            }
            else {
                self.contactImage.image = UIImage.init(named: "default profile")
            }
            self.contactName.text = model.name
            self.delegate = model.delegate
            self.contact = model
            self.deleteBtn.addTarget(self, action: #selector(self.deletebuttonTapped(sender:)), for: .touchUpInside)
        }
    }
    
    @objc func deletebuttonTapped(sender : UIButton) {
        self.delegate?.deleteButonTapped(item: self.contact!)
    }

}
