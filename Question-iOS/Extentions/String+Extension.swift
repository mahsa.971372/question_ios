//
//  String+Extension.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import Foundation


extension String {
    var localized : String {
        return NSLocalizedString(self, comment: "")
    }
}
