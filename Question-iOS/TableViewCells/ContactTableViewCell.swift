//
//  ContactTableViewCell.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//

import UIKit

public class ContactCellFactory : CellBehavior {
    public func getReuseId() -> String {
        return "CTC"
    }
    var id : String?
    var name : String
    var image : Data?
    var phoneNumber : String
    var delegate : deletButtonResponder?
    var selected : Bool = false
    
    init(item : Contacts , delegate : deletButtonResponder?) {
        self.name = item.name ?? ""
        self.image = (item.picture as? Data)
        self.phoneNumber = item.phoneNumber ?? ""
        self.id = item.id
        self.delegate = delegate
    }
    
}

class ContactTableViewCell: BaseTableViewCell {

    @IBOutlet weak var contactImage : UIImageView!
    @IBOutlet weak var contactName : UILabel!
    @IBOutlet weak var contactPhone : UILabel!
    @IBOutlet weak var selectState : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override func updateCell(cellModel: CellBehavior) {
        if let model = cellModel as? ContactCellFactory {
            if let data = model.image {
                self.contactImage.image = UIImage.init(data:data)
            }
            else {
                self.contactImage.image = UIImage.init(named: "default profile")
            }
            self.contactName.text = model.name
            self.contactPhone.text = model.phoneNumber
            if model.selected {
                self.selectState.image = UIImage.init(named: "select")
            }
            else {
                self.selectState.image = UIImage.init(named: "deselect")
            }
        }
    }
}
