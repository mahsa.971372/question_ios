//
//  Contacts+CoreDataClass.swift
//  Question-iOS
//
//  Created by Athena Yousefi on 10/12/18.
//  Copyright © 2018 Athena Yousefi. All rights reserved.
//
//

import Foundation
import CoreData
import Contacts

//public struct Contact {
//    var id : Int64
//    var name : String
//    var phoneNumber : String
//    var picture : String
//
//    init(id : Int64, name : String, phoneNumber : String, picture : String) {
//        self.id = id
//        self.name = name
//        self.phoneNumber = phoneNumber
//        self.picture = picture
//    }
//
//}

@objc(Contacts)
public class Contacts: NSManagedObject {
    
    public static func insertContacts(array : [CNContact]) {
        array.forEach{(item) in
            let newContact = NSEntityDescription.insertNewObject(forEntityName: "Contacts", into: CoreDataAPI.shared.persistentContainer.viewContext) as? Contacts
            newContact?.phoneNumber = item.phoneNumbers.first?.value.stringValue
            newContact?.id = item.identifier
            newContact?.name = item.givenName + " " + item.familyName
            if item.imageData != nil {
                newContact?.picture = NSData.init(data: item.imageData!)
            }
        }
        CoreDataAPI.shared.saveContext()
    }
    
    public static func loadContacts() -> [Contacts]? {
        let request : NSFetchRequest<Contacts> = Contacts.fetchRequest()
        do
        {
            let array = try CoreDataAPI.shared.persistentContainer.viewContext.fetch(request)
           return array
        }
        catch
        {
            print("error fetching result from core data")
            return nil
            
        }
    }
}
